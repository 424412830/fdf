/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/22 16:47:06 by abinet            #+#    #+#             */
/*   Updated: 2023/07/04 12:45:16 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H

# define FDF_H
# define ANGLE 0.7
# define LENGTH 1800
# define WIDTH 1000
# include "../libft/libft.h"
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <math.h>
# include "../mlx/mlx.h"

typedef struct s_data
{
	void	*img;
	char	*addr;
	int		bits_per_pixel;
	int		line_lenght;
	int		endian;
}				t_data;

typedef struct s_map
{
	int				nbr_col;
	int				nbr_lines;
	int				**altitude;
	int				set_altitude;
	unsigned int	**colors;
	unsigned int	zoom;
	float			angle;
	unsigned int	lenght;
	unsigned int	width;
	int				x_start;
	int				y_start;
	t_data			*img;
	void			*mlx;
	void			*mlx_win;
}					t_map;

typedef struct s_coordinates
{
	int				x;
	int				y;
	float			x1;
	float			y1;
	float			x2;
	float			y2;
	int				t1;
	int				t2;
	unsigned int	color;
}			t_coordinates;

int				read_map(char *file_name, t_map *datas);
void			my_mlx_pixel_put(t_data *data, int x, int y, int color);
size_t			strlen_without_space(char *next_line);
int				create_datas(char *file_name, t_map *datas);
void			free_tab(char **res);
void			free_tab_int(int **tab, t_map *datas);
void			free_tab_unsigned_int(unsigned int **tab, t_map *datas);
void			fdf(t_map *datas);
void			draw_map(t_map *datas);
void			draw_line(t_map *datas, t_coordinates *crd);
void			octant_1(t_map *datas, t_coordinates *crd);
void			octant_2(t_map *datas, t_coordinates *crd);
void			octant_3(t_map *datas, t_coordinates *crd);
void			octant_4(t_map *datas, t_coordinates *crd);
void			isometrie(t_map *datas, t_coordinates *crd);
int				key_hook(int key, t_map *datas);
int				mouse_hook(int mouse_hook, int x, int y, t_map *datas);
void			create_image(t_map *datas);
void			color(t_map *datas, t_coordinates *crd);
void			zoom(t_map *datas, t_coordinates *crd);
void			initialize_crd(t_coordinates *crd);
int				close_win_x(t_map *datas);
void			registers_hook(t_map *datas);
unsigned int	char_rgb_to_int(char *rgb);
unsigned int	char_to_int(char c);
int				fill_datas(t_map *datas, int fd);
int				memory_altitude_color(t_map *datas, int i, char *next_line);
unsigned int	find_colors(char *line);

#endif
