/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bresenham.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/21 12:23:21 by abinet            #+#    #+#             */
/*   Updated: 2023/06/29 15:19:40 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

void	octant_1(t_map *datas, t_coordinates *crd)
{
	float	error;
	float	dx;
	float	dy;
	float	x;
	float	y;

	dx = crd->x2 - crd->x1;
	dy = crd->y2 - crd->y1;
	error = 0;
	x = crd->x1;
	y = crd->y1;
	while (x < crd->x2)
	{
		my_mlx_pixel_put(datas->img, x, y, crd->color);
		error += dy;
		if (2 * error > dx)
		{
			error -= dx;
			y++;
		}
		x++;
	}
}

void	octant_2(t_map *datas, t_coordinates *crd)
{
	float	error;
	float	dx;
	float	dy;
	float	x;
	float	y;

	dx = crd->x2 - crd->x1;
	dy = crd->y2 - crd->y1;
	error = 0;
	x = crd->x1;
	y = crd->y1;
	while (y < crd->y2)
	{
		my_mlx_pixel_put(datas->img, x, y, crd->color);
		error += dx;
		if (2 * error > dy)
		{
			error -= dy;
			x++;
		}
		y++;
	}
}

void	octant_3(t_map *datas, t_coordinates *crd)
{
	float	error;
	float	dx;
	float	dy;
	float	x;
	float	y;

	dx = crd->x1 - crd->x2;
	dy = crd->y2 - crd->y1;
	error = 0;
	x = crd->x1;
	y = crd->y1;
	while (y < crd->y2)
	{
		my_mlx_pixel_put(datas->img, x, y, crd->color);
		error += dx;
		if (2 * error > dy)
		{
			error -= dy;
			x--;
		}
		y++;
	}
}

void	octant_4(t_map *datas, t_coordinates *crd)
{
	float	error;
	float	dx;
	float	dy;
	float	x;
	float	y;

	dx = crd->x1 - crd->x2;
	dy = crd->y2 - crd->y1;
	error = 0;
	x = crd->x1;
	y = crd->y1;
	while (crd->x2 < x)
	{
		my_mlx_pixel_put(datas->img, x, y, crd->color);
		error += dy;
		if (2 * error > dx)
		{
			error -= dx;
			y++;
		}
		x--;
	}
}
