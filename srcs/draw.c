/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/23 19:14:11 by abinet            #+#    #+#             */
/*   Updated: 2023/06/30 11:48:29 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

void	draw_map(t_map *datas)
{
	t_coordinates	*crd;

	create_image(datas);
	crd = malloc(sizeof(t_coordinates));
	crd->x = -1;
	while (++crd->x < (datas->nbr_col))
	{
		crd->y = -1;
		while (++crd->y < (datas->nbr_lines))
		{
			initialize_crd(crd);
			if (crd->x < (datas->nbr_col - 1))
			{
				crd->x2++;
				draw_line(datas, crd);
			}
			initialize_crd(crd);
			if (crd->y < (datas->nbr_lines - 1))
			{
				crd->y2++;
				draw_line(datas, crd);
			}
		}
	}
	free(crd);
}

void	draw_line(t_map *datas, t_coordinates *crd)
{
	color(datas, crd);
	isometrie(datas, crd);
	zoom(datas, crd);
	if (crd->y2 < crd->y1)
	{
		ft_swap(&crd->y2, &crd->y1);
		ft_swap(&crd->x2, &crd->x1);
	}
	if (crd->x1 < crd->x2)
	{
		if ((crd->y2 - crd->y1) <= (crd->x2 - crd->x1))
			octant_1(datas, crd);
		else
			octant_2(datas, crd);
	}
	else
	{
		if ((crd->y2 - crd->y1) < (crd->x1 - crd->x2))
			octant_4(datas, crd);
		else
			octant_3(datas, crd);
	}
}
