/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/04/03 10:48:40 by abinet            #+#    #+#             */
/*   Updated: 2023/07/04 14:18:32 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

void	fdf(t_map *datas)
{
	datas->img = malloc(sizeof(t_data));
	if (!datas->img)
		return ;
	datas->mlx = mlx_init();
	datas->mlx_win = mlx_new_window(datas->mlx, LENGTH, WIDTH, "fdf");
	datas->zoom = fmin((LENGTH / 2) / datas->nbr_col,
			(WIDTH / 2) / datas->nbr_lines);
	datas->x_start = LENGTH / 2;
	datas->y_start = WIDTH / 8;
	datas->set_altitude = 10;
	datas->angle = 0.7;
	draw_map(datas);
	mlx_put_image_to_window(datas->mlx, datas->mlx_win,
		datas->img->img, 10, 10);
	registers_hook(datas);
	mlx_loop(datas->mlx);
	mlx_destroy_image(datas->mlx, datas->img->img);
	mlx_destroy_window(datas->mlx, datas->mlx_win);
	mlx_destroy_display(datas->mlx);
	free(datas->mlx);
	free(datas->img);
}

int	main(int argc, char **argv)
{
	t_map	*datas;

	(void)argc;
	(void)argv;
	if (argc != 2)
		return (ft_printf("number arg error\n", 0));
	datas = malloc(sizeof(t_map));
	if (!datas)
		return (ft_printf("error memory creating datas\n"), 0);
	if (read_map(argv[1], datas) == -1)
		return (ft_printf("no valid file\n"), 0);
	if (create_datas(argv[1], datas) == -1)
		return (ft_printf("error memory in function create_datas\n"), 0);
	fdf(datas);
	free_tab_int(datas->altitude, datas);
	free_tab_unsigned_int(datas->colors, datas);
	free(datas);
	return (0);
}
