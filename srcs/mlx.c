/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/31 16:50:14 by abinet            #+#    #+#             */
/*   Updated: 2023/07/04 12:36:45 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

void	my_mlx_pixel_put(t_data *data, int x, int y, int color)
{
	char	*dst;

	if (x < 0 || y < 0 || x > LENGTH || y > WIDTH)
		return ;
	dst = data->addr + (y * data->line_lenght + x * (data->bits_per_pixel / 8));
	*(unsigned int *) dst = color;
}

void	create_image(t_map *datas)
{
	datas->img->img = mlx_new_image(datas->mlx, LENGTH, WIDTH);
	datas->img->addr = mlx_get_data_addr(datas->img->img,
			&datas->img->bits_per_pixel,
			&datas->img->line_lenght, &datas->img->endian);
}
