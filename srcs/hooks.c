/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/29 17:51:32 by abinet            #+#    #+#             */
/*   Updated: 2023/07/04 14:11:39 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

void	registers_hook(t_map *datas)
{
	mlx_key_hook(datas->mlx_win, key_hook, datas);
	mlx_hook(datas->mlx_win, 17, 0L, close_win_x, datas);
	mlx_mouse_hook(datas->mlx_win, mouse_hook, datas);
}

int	key_hook(int keycode, t_map *datas)
{
	if (keycode == 65307 || keycode == 17)
		mlx_loop_end(datas->mlx);
	if (keycode == 65362)
		datas->y_start += 20;
	if (keycode == 65361)
		datas->x_start += 20;
	if (keycode == 65363)
		datas->x_start -= 20;
	if (keycode == 65364)
		datas->y_start -= 20;
	if (keycode == 110)
		datas->set_altitude += 1;
	if (keycode == 109)
		datas->set_altitude -= 1;
	if (keycode == 122)
		datas->angle += 0.1;
	if (keycode == 120)
		datas->angle -= 0.1;
	printf("%d\n", keycode);
	mlx_destroy_image(datas->mlx, datas->img->img);
	draw_map(datas);
	mlx_put_image_to_window(datas->mlx, datas->mlx_win,
		datas->img->img, 10, 10);
	return (0);
}

// int	key_hook(int keycode, t_map *datas)
// {
// 	if ((keycode >= 65361 && keycode <= 65364) || keycode == 4 || keycode == 5
// 		|| keycode == 110 || keycode == 109)
// 	{
// 		if (keycode == 65362)
// 			datas->y_start += 10;
// 		if (keycode == 65361)
// 			datas->x_start += 10;
// 		if (keycode == 65363)
// 			datas->x_start -= 10;
// 		if (keycode == 65364)
// 			datas->y_start -= 10;
// 		if (keycode == 110)
// 			datas->set_altitude += 1;
// 		if (keycode == 109)
// 			datas->set_altitude -= 1;
// 		mlx_destroy_image(datas->mlx, datas->img->img);
// 		draw_map(datas);
// 		mlx_put_image_to_window(datas->mlx, datas->mlx_win,
// 			datas->img->img, 10, 10);
// 	}
// 	if (keycode == 65307 || keycode == 17)
// 		mlx_loop_end(datas->mlx);
// 	return (0);
// }

int	mouse_hook(int mouse_hook, int x, int y, t_map *datas)
{
	(void)x;
	(void)y;
	if (mouse_hook == 4)
		datas->zoom -= 1;
	if (mouse_hook == 5)
		datas->zoom += 1;
	mlx_destroy_image(datas->mlx, datas->img->img);
	draw_map(datas);
	mlx_put_image_to_window(datas->mlx, datas->mlx_win,
		datas->img->img, 10, 10);
	return (0);
}

int	close_win_x(t_map *datas)
{
	mlx_loop_end(datas->mlx);
	return (0);
}
