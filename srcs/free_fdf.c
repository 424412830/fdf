/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/03 17:47:37 by abinet            #+#    #+#             */
/*   Updated: 2023/07/04 11:47:11 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

void	free_tab_int(int **tab, t_map *datas)
{
	int	i;

	i = 0;
	while (i < datas->nbr_lines)
	{
		free(tab[i]);
		i++;
	}
	free(tab);
}

void	free_tab_unsigned_int(unsigned int **tab, t_map *datas)
{
	int	i;

	i = 0;
	while (i < datas->nbr_lines)
	{
		free(tab[i]);
		i++;
	}
	free(tab);
}

void	free_tab(char **res)
{
	int	i;

	i = 0;
	while (res[i])
	{
		free(res[i]);
		i++;
	}
	free(res);
}
