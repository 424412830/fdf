/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/29 15:55:31 by abinet            #+#    #+#             */
/*   Updated: 2023/07/04 12:39:50 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

void	isometrie(t_map *datas, t_coordinates *crd)
{
	float	temp;
	float	temp2;

	crd->x1 *= datas->set_altitude;
	crd->y1 *= datas->set_altitude;
	crd->x2 *= datas->set_altitude;
	crd->y2 *= datas->set_altitude;
	temp = crd->x1;
	temp2 = crd->x2;
	crd->x1 = (crd->x1 - crd->y1) * cos(datas->angle) / datas->set_altitude;
	crd->y1 = ((temp + crd->y1) * sin(datas->angle) - crd->t1)
		/ datas->set_altitude;
	crd->x2 = (crd->x2 - crd->y2) * cos(datas->angle)
		/ datas->set_altitude;
	crd->y2 = ((temp2 + crd->y2) * sin(datas->angle) - crd->t2)
		/ datas->set_altitude;
}

void	color(t_map *datas, t_coordinates *crd)
{
	unsigned int	color_x1;
	unsigned int	color_x2;

	color_x1 = datas->colors[(int)crd->y1][(int)crd->x1];
	color_x2 = datas->colors[(int)crd->y2][(int)crd->x2];
	crd->t1 = datas->altitude[(int)crd->y1][(int)crd->x1];
	crd->t2 = datas->altitude[(int)crd->y2][(int)crd->x2];
	if (color_x1 > 0)
		(crd->color = color_x1);
	else if (color_x2 > 0)
		(crd->color = color_x2);
	else if (crd->t1 == 0 || crd->t2 == 0)
		crd->color = 0x00FF00;
	else if (crd->t1 < 0 && crd->t2 < 0)
		crd->color = 0x0000FF;
	else if (crd->t1 > 0 && crd->t2 > 0)
		crd->color = 0xFFFF00;
	else
		crd->color = 0x00FF00;
}

void	zoom(t_map *datas, t_coordinates *crd)
{
	crd->x1 = crd->x1 * datas->zoom + (datas->x_start);
	crd->y1 = crd->y1 * datas->zoom + (datas->y_start);
	crd->x2 = crd->x2 * datas->zoom + (datas->x_start);
	crd->y2 = crd->y2 * datas->zoom + (datas->y_start);
}

void	initialize_crd(t_coordinates *crd)
{
	crd->x1 = crd->x;
	crd->y1 = crd->y;
	crd->x2 = crd->x;
	crd->y2 = crd->y;
}
