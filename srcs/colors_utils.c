/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/04 11:46:40 by abinet            #+#    #+#             */
/*   Updated: 2023/07/04 12:12:37 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

unsigned int	find_colors(char *line)
{
	unsigned int	color;
	int				i;
	char			*char_color;

	i = 0;
	color = 0;
	while (line[i])
	{
		if (line[i] == ',')
		{
			char_color = ft_substr(line, i + 1, 8);
			color = char_rgb_to_int(char_color);
			free(char_color);
			return (color);
		}
		i++;
	}
	return (0);
}

unsigned int	char_to_int(char c)
{
	unsigned int	nbr;

	nbr = 0;
	if (ft_isdigit(c) != 0)
		nbr = c - 48;
	else if (ft_isalpha(c) != 0 && c < 92)
		nbr = c - 55;
	else if (ft_isalpha(c) != 0 && c > 92)
		nbr = c - 88;
	return (nbr);
}

unsigned int	char_rgb_to_int(char *rgb)
{
	unsigned int	nbr;
	unsigned int	blue;
	unsigned int	red;
	unsigned int	green;
	int				len_rgb;

	len_rgb = ft_strlen(rgb);
	red = 0;
	green = 0;
	blue = 0;
	if (rgb[0] != '0' && rgb[1] + 1 != 'x')
		return ((printf("error color\n"), 1));
	if (len_rgb >= 3)
		red = char_to_int(rgb[2]) * 16 + char_to_int(rgb[3]);
	if (len_rgb >= 5)
		green = char_to_int(rgb[4]) * 16 + char_to_int(rgb[5]);
	if (len_rgb >= 7)
		blue = char_to_int(rgb[6]) * 16 + char_to_int(rgb[7]);
	nbr = (red * 65536) + (green * 256) + blue;
	return (nbr);
}
