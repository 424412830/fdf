/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/04/17 14:16:34 by abinet            #+#    #+#             */
/*   Updated: 2023/07/04 11:54:23 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

size_t	strlen_without_space(char *next_line)
{
	size_t	nbr;
	int		i;

	i = 0;
	nbr = 0;
	while (next_line[i])
	{
		if (next_line[i] != ' ' && next_line[i] != '\n'
			&& (next_line[i + 1] == ' ' || next_line[i + 1] == '\n'))
			nbr++;
		i++;
	}
	return (nbr);
}

int	read_map(char *file_name, t_map *datas)
{
	char	*next_line;
	int		fd;
	size_t	nbr_lines;

	fd = open(file_name, O_RDONLY);
	if (fd < 0)
		return (-1);
	next_line = get_next_line(fd);
	if (!next_line)
		return (-1);
	datas->nbr_col = strlen_without_space(next_line);
	nbr_lines = 0;
	while (next_line && ++nbr_lines)
	{
		free(next_line);
		next_line = get_next_line(fd);
	}
	datas->nbr_lines = nbr_lines;
	free(next_line);
	close(fd);
	return (0);
}

int	create_datas(char *file_name, t_map *datas)
{
	int		fd;

	datas->altitude = malloc(sizeof(int *) * (datas->nbr_lines) + 1);
	if (!datas->altitude)
		return (-1);
	datas->colors = malloc(sizeof(unsigned int *) * (datas->nbr_lines) + 1);
	if (!datas->colors)
		return (-1);
	fd = open(file_name, O_RDONLY);
	if (fill_datas(datas, fd))
		return (close (fd), -1);
	close(fd);
	return (0);
}

int	fill_datas(t_map *datas, int fd)
{
	int		i;
	int		j;
	char	*next_line;
	char	**altitude_char;

	i = 0;
	while (i < datas->nbr_lines)
	{
		next_line = get_next_line(fd);
		altitude_char = ft_split(next_line, ' ');
		j = 0;
		if (memory_altitude_color(datas, i, next_line) != 0)
			return (free(altitude_char), -1);
		while (j < datas->nbr_col)
		{
			datas->altitude[i][j] = ft_atoi(altitude_char[j]);
			datas->colors[i][j] = find_colors(altitude_char[j]);
			j++;
		}
		i++;
		free(next_line);
		free_tab(altitude_char);
	}
	return (0);
}

int	memory_altitude_color(t_map *datas, int i, char *next_line)
{
	datas->altitude[i] = malloc(sizeof(int) * datas->nbr_col + 1);
	if (!datas->altitude[i])
		return (free_tab_int(datas->altitude, datas),
			free(next_line), -1);
	datas->colors[i] = malloc(sizeof(unsigned int) * datas->nbr_col + 1);
	if (!datas->colors[i])
		return (free_tab_int(datas->altitude, datas),
			free(next_line),
			free_tab_unsigned_int(datas->colors, datas), -1);
	return (0);
}
