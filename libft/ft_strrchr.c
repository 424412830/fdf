/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 22:49:39 by abinet            #+#    #+#             */
/*   Updated: 2022/12/03 10:51:09 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <stddef.h>
#include <string.h>

char	*ft_strrchr(const char *str, int c)
{
	int		i;
	char	cc;

	cc = (char)c;
	i = ft_strlen(str);
	if (cc == '\0')
		return ((char *)&str[i]);
	while (i-- > 0)
	{
		if (str[i] == cc)
			return ((char *)&str[i]);
	}
	return (NULL);
}
/*
int	main(int argc, char **argv)
{
	(void) argc;
	printf("%p\n", ft_strrchr(argv[1], argv[2][0]));
	printf("%p\n", strrchr(argv[1], argv[2][0]));
	return (0);
}
*/
