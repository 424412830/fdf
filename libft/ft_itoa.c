/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/16 17:32:17 by abinet            #+#    #+#             */
/*   Updated: 2022/11/28 18:16:20 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

static long	nbrlen(long n)
{
	int		len;

	if (n < 0)
		n = n * -1;
	len = 1;
	while (n > 9)
	{
		n = n / 10;
		len++;
	}
	return (len);
}

char	*ft_itoa(int n)
{
	char	*res;
	int		len;
	int		i;
	long	ln;

	ln = (long)n;
	i = 0;
	if (ln < 0)
	{
		ln = ln * -1;
		i = 1;
	}
	len = nbrlen(ln) + i;
	res = malloc(sizeof(char) * len + 1);
	if (!res)
		return (NULL);
	res[len] = '\0';
	while (--len >= (i))
	{
		res[len] = ln % 10 + 48;
		ln = ln / 10;
	}
	if (i == 1)
		res[0] = '-';
	return (res);
}
/*
int	main(int argc, char **argv)
{
	(void) argc;
	(void) argv;
	printf("%s\n", ft_itoa(atoi(argv[1])));
	//printf("%ld\n", nbrlen(atoi(argv[1])));
	return (0);
}

*/
