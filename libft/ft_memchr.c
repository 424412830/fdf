/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/16 14:50:44 by abinet            #+#    #+#             */
/*   Updated: 2022/11/28 17:12:37 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	*ft_memchr(const void *s, int c, size_t n)
{
	long unsigned int	i;
	unsigned char		*str;

	str = (unsigned char *) s;
	i = 0;
	while (i < n)
	{
		if (str[i] == (unsigned char)c)
			return (&str[i]);
		i++;
	}
	return (NULL);
}
/*
int	main(int argc, char **argv)
{
	int	a;

	(void) argc;
	a = 12;
	printf("vrai memchr = %p\n", (char *)memchr(argv[1], a, 10));
	printf("ft_memchr = %p\n", (char *)ft_memchr(argv[1], a, 10));
	return (0);
}
*/
