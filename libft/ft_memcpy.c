/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/16 13:40:41 by abinet            #+#    #+#             */
/*   Updated: 2022/12/01 10:07:42 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void	*ft_memcpy(void *dest, const void *src, size_t size)
{
	unsigned int	i;
	char			*src2;
	char			*dest2;

	if (!dest && !src)
		return (NULL);
	src2 = (char *)src;
	dest2 = (char *)dest;
	i = 0;
	while (i < size)
	{
		dest2[i] = src2[i];
		i++;
	}
	return (dest);
}
/*
int	main(int argc, char **argv)
{
	(void)argc;
	char	*dest;
	dest = malloc(sizeof(char)*10);
	if(!dest)
		return (0);
	ft_memcpy(dest, argv[1], 0);
	printf("%p\n", dest);
	free(dest);
	return (0);
}
*/
