/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/09 20:53:42 by abinet            #+#    #+#             */
/*   Updated: 2022/12/01 14:28:41 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <bsd/string.h>

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	i;
	size_t	a;
	size_t	b;

	if (size == 0 || size <= ft_strlen(dst))
		return (ft_strlen(src) + size);
	a = ft_strlen(dst);
	b = ft_strlen(src);
	i = 0;
	while (i < (size - a - 1) && src[i])
	{
		dst[a + i] = src[i];
		i++;
	}
	dst[a + i] = '\0';
	return (a + b);
}
/*
int	main(int argc, char **argv)
{
	char	*dst;
	char	*dest;
	size_t	r1;
	size_t	r2;

	dst = argv[2];
	dest = argv[2];
	(void) argc;
	r1 = ft_strlcat(dst,argv[1],atoi(argv[3]));
	r2 = strlcat(dest,argv[1],atoi(argv[3]));
	printf("ft_strlcat dst = %s\n", dst);
	printf("strlcat dest = %s\n", dest);
	printf("ft_strlcat return = %zu\n", r1);
	printf("strlcat return = %zu\n", r2);
	return (0);
}*/
