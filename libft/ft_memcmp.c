/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/16 14:51:07 by abinet            #+#    #+#             */
/*   Updated: 2022/11/23 23:03:51 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char	*str1;
	unsigned char	*str2;

	str1 = (unsigned char *)s1;
	str2 = (unsigned char *)s2;
	if (n == 0)
		return (0);
	while (n > 1 && (*str1 == *str2))
	{
		str1++;
		str2++;
		n--;
	}
	return (*str1 - *str2);
}

/*int	main(int argc, char **argv)
{
	(void) argc;
	printf("%d\n", ft_memcmp(argv[1], argv[2], atoi(argv[3])));
	printf("%d\n", memcmp(argv[1], argv[2], atoi(argv[3])));
	return (0);
}
*/
