NAME		= fdf

################## FILES & DIRECTORIES ###################################################

SRCS_FILES	= fdf.c	parsing.c	mlx.c	bresenham.c	draw.c	draw_utils.c	hooks.c	free_fdf.c	colors_utils.c
SRCS_DIR	= srcs/
SRCS		= $(addprefix $(SRCS_DIR),$(SRCS_FILES))


OBJS_FILES	= $(SRCS:.c=.o)
OBJS_DIR	= objs/
#OBJS		= $(addprefix $(OBJS_DIR),$(OBJS_FILES))

HEADER_DIR		= include/
HEADER_FILE		= fdf.h
HEADER			= $(addprefix ${HEADER_DIR}, ${HEADER_FILE})

LIBFT		= libft/libft.a
LIBFT_PATH	= libft/

########################################################################################

CC			= cc

FLAGS		= -Wall -Wextra -Werror

FLAGS_MINILIBX	= -Lmlx_linux -lmlx_Linux -L./mlx -Imlx_linux -lXext -lX11 -lm -lz

RM			= rm -rf


###################### RULES ##########################################################


%.o: %.c
		${CC} ${FLAGS} -c $< -o ${<:.c=.o}

${NAME}:	${OBJS_FILES} ${HEADER} ${LIBFT}
		${CC} ${OBJS_FILES} ${LIBFT} ${FLAGS_MINILIBX} -o ${NAME}
		mkdir -p ${OBJS_DIR}
		mv ${OBJS_FILES} ${OBJS_DIR}

${LIBFT}:
		make -C ${LIBFT_PATH}

all:	${NAME}

clean :
		make -C ${LIBFT_PATH} clean
		${RM} ${OBJS_DIR}

fclean : clean
		make -C ${LIBFT_PATH} fclean
		${RM} ${NAME}

re : fclean all

.SECONDARY : $(OBJS_FILES)

.PHONY : all clean fclean re bonus so
